package com.nTier.Travel;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CommuterTest {
	public Passenger commuter;
	public Passenger vacationer;
	@Before
	public void doSetup()
	{
		commuter=new Commuter(3,false,4);
		vacationer=new Vacationer(1900,2);
	}

	@Test
	public void test() {
		commuter.cost();
		assertEquals(1.5,commuter.getCost(),000);
	}
	@Test
	public void totalNewsPapers()
	{
		
		assertEquals(6,Passenger.totalNewspapers,0);
	}

	@After
	public void end()
	{   
		commuter=null;
		vacationer=null;
	}
}
