package com.nTier.Travel;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class VacationerTest {
	@Rule
	public ExpectedException exception=ExpectedException.none();
	public Passenger vacationer;
	public Passenger commuter;
	@Before
	public void doSetup() {
		vacationer=new Vacationer(90,1);
		commuter=new Commuter(3,false,2);
	}

	@Test
	public void costCalc() {
		vacationer.cost();
		assertEquals(45.0,vacationer.getCost(),0.001);
	}
	
	@Test
	public void validMiles()
	{
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage("Miles");
		Passenger v1=new Vacationer(5000,0);
		Vacationer v=(Vacationer)v1;
			v.validateMiles();
	}
	
	@Test
	public void mealsCount()
	{
		Vacationer v=(Vacationer)vacationer;
		assertEquals(1,v.getMeals(),0.00);
	}
	
	@Test
	public void totalCost()
	{
		assertEquals(1951,Passenger.totalFare,0.0);
	}
	@After
	public void cleanUp()
	{
		vacationer=null;
		commuter=null;
	}

}

