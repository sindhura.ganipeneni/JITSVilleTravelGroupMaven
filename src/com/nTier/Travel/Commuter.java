package com.nTier.Travel;

public class Commuter extends Passenger{
	
	public Commuter(int rateFactor,boolean frequentFlier,int newsPapers)
	{
		this.setRateFactor(rateFactor);
		this.setFrequentFlier(frequentFlier);
		this.setNewsPapers(newsPapers);
		this.calcTotalNewspapers();
		this.cost();
	}

	
	
	@Override
	public void cost() {
		this.setCost(this.getRateFactor()*0.5);
		if("true".equals(this.isFrequentFlier()))
		{
			this.setCost((this.getRateFactor()*0.5)-(this.getCost()*0.1));
		}
		totalFare+= this.getCost();
	}



	@Override
	public void display() {
		System.out.println("Commuter - Stops: "+this.getRateFactor() +" Number of Newspapers : "+ this.getNewsPapers()+ " Frequent flier : " + this.isFrequentFlier()+ " Cost : "+this.getCost());
		
	}


}
