package com.nTier.Travel;

public class Vacationer extends Passenger implements Meals{
	
	private int meals;
	
	public void setMeals(int meals) {
		this.meals = meals;
	}

	public Vacationer(int rateFactor,int newsPapers)
	{
		this.setRateFactor(rateFactor);
		this.setNewsPapers(newsPapers);
		this.calcTotalNewspapers();
		this.cost();
		this.validateMiles();
		this.calculateMeals();
	}
	
	public int getMeals()
	{
		int mealsCount;
		mealsCount=this.calculateMeals();
		return mealsCount;
	}
	@Override
	public int calculateMeals() {
		int mealCount=this.getRateFactor()/100;
		int remaining=this.getRateFactor()%100;
		if(remaining>50)
		{
			mealCount+=1;
		}
		this.setMeals(mealCount);
		return mealCount;
	}
	@Override
	public void cost() {
		this.setCost(this.getRateFactor()*0.5);	
		totalFare+=(this.getRateFactor()*0.5);
	}
	
	public void validateMiles()
	{
		if((this.getRateFactor()<5)||(this.getRateFactor()>4000))
		{
			throw new IllegalArgumentException("Miles "+this.getRateFactor()+" should not be less than 5 or more than 4000");
		}
		
	}

	@Override
	public void display() {
		System.out.println("Miles : "+this.getRateFactor()+"Number of Newspapers : "+this.getNewsPapers()+" Meals : "+this.getMeals()+" Cost : "+this.getCost());
		
	}
}
