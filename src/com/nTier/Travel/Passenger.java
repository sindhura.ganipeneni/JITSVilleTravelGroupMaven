package com.nTier.Travel;

public abstract class Passenger {
	
	protected final double rate=0.5;
	
	private int newsPapers;
	static double totalFare;
	private int rateFactor;
	static int totalNewspapers;
	private boolean frequentFlier;
	private double cost;
	
	public static int gettotalNewspapers()
	{
		return totalNewspapers;
	}
	public void calcTotalNewspapers()
	{
		totalNewspapers+=this.getNewsPapers();
	}
	public static double getTotalFare() {
		return totalFare;
	}
	public int getNewsPapers() {
		return newsPapers;
	}
	public void setNewsPapers(int newsPapers) {
		this.newsPapers = newsPapers;
	}
	public boolean isFrequentFlier() {
		return frequentFlier;
	}
	public void setFrequentFlier(boolean frequentFlier) {
		this.frequentFlier = frequentFlier;
	}
	public int getRateFactor() {
		return rateFactor;
	}
	protected void setRateFactor(int rateFactor) {
		this.rateFactor = rateFactor;
	}
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	public double getRate() {
		return rate;
	}
	
	public abstract void cost();
	public abstract void display();
}
