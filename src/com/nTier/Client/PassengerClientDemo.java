package com.nTier.Client;

import java.util.ArrayList;

import com.nTier.Travel.Passenger;



class PassengerClientDemo {

	public static void main(String[] args) {
		Passenger p1=PassengerFactory.getCommuter(3,true,0);
		Passenger p2=PassengerFactory.getCommuter(5,true,0);
		Passenger p3=PassengerFactory.getCommuter(4,false,1);
		Passenger p4=PassengerFactory.getVacationer(90,0);
		Passenger p5=PassengerFactory.getVacationer(199,1);
		ArrayList<Passenger> array=new ArrayList<>();
		array.add(p1);
		array.add(p2);
		array.add(p3);
		array.add(p4);
		array.add(p5);
		for(Passenger p:array)
		{
			p.display();
		}
		System.out.println("Total Fare : "+Passenger.getTotalFare());
		System.out.println("Total Newspapers count : "+Passenger.gettotalNewspapers());
		
}
}
