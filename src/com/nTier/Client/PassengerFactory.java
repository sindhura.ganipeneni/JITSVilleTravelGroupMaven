package com.nTier.Client;

import com.nTier.Travel.*;
class PassengerFactory {
static Passenger getCommuter(int rateFactor,boolean frequentFlier,int newsPapers)
{
	return new Commuter(rateFactor,frequentFlier,newsPapers);
}
static Passenger getVacationer(int rateFactor,int newsPapers)
{
	return new Vacationer(rateFactor,newsPapers);
}
}
